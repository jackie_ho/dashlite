package dashlite.doordash.jho.com.dashlite

import com.nhaarman.mockitokotlin2.*
import dashlite.doordash.jho.com.dashlite.list.*
import dashlite.doordash.jho.com.dashlite.model.Restaurant
import dashlite.doordash.jho.com.dashlite.utils.SchedulerProvider
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import io.reactivex.subjects.PublishSubject
import junit.framework.Assert.*
import org.junit.Before
import org.junit.Test
import java.io.IOException

/**
 * Created by JH on 1/13/19.
 */
class RestaurantListViewModelTest {

    private lateinit var viewModel: RestaurantListViewModel
    private lateinit var useCase: LoadRestaurantListUseCase

    private val testScheduler = TestScheduler()
    private val testSchedulerProvider = TestSchedulerProvider(testScheduler)
    private val uiSubject = PublishSubject.create<RestaurantListUiEvent>()
    private val actionSubject = PublishSubject.create<RestaurantListAction>()

    private val restaurantRepo: RestaurantListRepository = mock()
    private val expectedRestaurantList = arrayListOf(
        Restaurant(id = 1, name = "McDonnas", description =  "Fast-food", cover_img_url = "", status = "30 min", delivery_fee = 0.0),
        Restaurant(id = 1, name = "Sushi Ristorante", description =  "Italian Sushi", cover_img_url = "", status = "Closed", delivery_fee = 0.0),
        Restaurant(id = 1, name = "Artisan Pizza", description =  "Pizza", cover_img_url = "", status = "10 min", delivery_fee = 0.0),
        Restaurant(id = 1, name = "Express Panda", description =  "Panda delicacy", cover_img_url = "", status = "30 min", delivery_fee = 100.0),
        Restaurant(id = 1, name = "Fruit", description =  "Fruit", cover_img_url = "", status = "120 min", delivery_fee = 0.0)
    )

    @Before
    fun setup() {
        whenever(restaurantRepo.restaurantList(any(), any(), any(), any())).thenReturn(Observable.fromArray(expectedRestaurantList))
        useCase = LoadRestaurantListUseCase(restaurantRepo, testSchedulerProvider)
        viewModel = RestaurantListViewModel(useCase, testSchedulerProvider)
        viewModel.bindInput(uiSubject)
    }

    @Test
    fun testLoadAction() {
        val testObserver = TestObserver<RestaurantListAction>()

        viewModel.actionSubject.subscribe(testObserver)

        uiSubject.onNext(RestaurantListUiEvent(23.44, -100.2))
        testScheduler.triggerActions()

        testObserver.assertNoErrors()
        testObserver.assertNotComplete()
        testObserver.assertValueCount(1)
        val actions  = testObserver.values()
        assertTrue(actions[0] is RestaurantListAction.Load)
        assertEquals(23.44, (actions[0] as RestaurantListAction.Load).lat)
        assertEquals(-100.2, (actions[0] as RestaurantListAction.Load).lon)
        verify(restaurantRepo, times(1)).restaurantList(23.44, -100.2, 0, 50)
    }

    @Test
    fun testLoadResultSuccess() {
        val testObserver = TestObserver<RestaurantListResult>()

        useCase.execute(actionSubject).subscribe(testObserver)
        actionSubject.onNext(RestaurantListAction.Load(23.44, -100.2, 0, 5))
        testScheduler.triggerActions()

        testObserver.assertNoErrors()
        testObserver.assertNotComplete()
        testObserver.assertValueCount(2)

        val result  = testObserver.values()
        assertTrue(result[0] is RestaurantListResult.Loading)
        assertTrue(result[1] is RestaurantListResult.Success)

        verify(restaurantRepo, times(1)).restaurantList(23.44, -100.2, 0, 5)
    }

    @Test
    fun testLoadResultError() {
        val testObserver = TestObserver<RestaurantListResult>()
        whenever(restaurantRepo.restaurantList(any(), any(), any(), any())).thenReturn(Observable.error(IOException("No Network")))

        useCase.execute(actionSubject).subscribe(testObserver)
        actionSubject.onNext(RestaurantListAction.Load(23.44, -100.2, 0, 5))
        testScheduler.triggerActions()

        testObserver.assertNoErrors()
        testObserver.assertNotComplete()
        testObserver.assertValueCount(2)

        val result  = testObserver.values()
        assertTrue(result[0] is RestaurantListResult.Loading)
        assertTrue(result[1] is RestaurantListResult.Error)

        verify(restaurantRepo, times(1)).restaurantList(23.44, -100.2, 0, 5)
    }

    @Test
    fun testRender() {
        val testObserver = TestObserver<RestaurantListViewState>()

        viewModel.render().subscribe(testObserver)
        viewModel.bindInput(uiSubject)

        uiSubject.onNext(RestaurantListUiEvent(23.44, -100.2))
        testScheduler.triggerActions()

        testObserver.assertNoErrors()
        testObserver.assertNotComplete()
        testObserver.assertValueCount(3)

        val states = testObserver.values()
        assertTrue(states[1].isLoading)
        assertEquals(5, states[2].list.size)
        assertEquals("Artisan Pizza", states[2].list[2].name)
        assertFalse(states[2].isLoading)
        assertFalse(states[2].isError)
    }


}


class TestSchedulerProvider(private val testScheduler: TestScheduler): SchedulerProvider {
    override fun io(): Scheduler = testScheduler

    override fun main(): Scheduler = testScheduler

    override fun computation(): Scheduler = testScheduler
}