package dashlite.doordash.jho.com.dashlite.model

/**
 * Created by JH on 1/13/19.
 */
data class Restaurant(val id: Int,
                      val name: String,
                      val description: String,
                      val cover_img_url: String,
                      val status: String,
                      val delivery_fee: Double)