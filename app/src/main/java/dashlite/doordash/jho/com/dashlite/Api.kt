package dashlite.doordash.jho.com.dashlite

import dashlite.doordash.jho.com.dashlite.model.Restaurant
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by JH on 1/13/19.
 */
interface Api {

    @GET("v2/restaurant/")
    fun restaurantList(@Query("lat") lat: Double,
                       @Query("lng") lon: Double,
                       @Query("offset") offset: Int,
                       @Query("limit") limit: Int): Observable<List<Restaurant>>

}