package dashlite.doordash.jho.com.dashlite.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dashlite.doordash.jho.com.dashlite.utils.AppSchedulerProvider
import dashlite.doordash.jho.com.dashlite.utils.SchedulerProvider
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject

/**
 * Created by JH on 1/12/19.
 */
class RestaurantListViewModel(loadUseCase: LoadRestaurantListUseCase, schedulerProvider: SchedulerProvider) : ViewModel() {

    private val disposables = CompositeDisposable()
    private val renderSubject = BehaviorSubject.create<RestaurantListViewState>()
    val actionSubject = PublishSubject.create<RestaurantListAction>()

    init {
        disposables.add(
            loadUseCase.execute(actionSubject)
                .scan(RestaurantListViewState(isIdle = true), ::reduceState)
                .observeOn(schedulerProvider.main())
                .subscribe(renderSubject::onNext, ::handleError)
        )
    }

    fun render(): Observable<RestaurantListViewState> = renderSubject

    fun bindInput(inputObservable: Observable<RestaurantListUiEvent>) {
        disposables.add(inputObservable
            .subscribe(::loadRestaurantList))
    }

    private fun reduceState(prevState: RestaurantListViewState, next: RestaurantListResult): RestaurantListViewState {
        return when(next) {
            is RestaurantListResult.Loading -> prevState.copy(isLoading = true, isError = false, isIdle = false)
            is RestaurantListResult.Success -> RestaurantListViewState(list = next.restaurantList, isIdle = false)
            is RestaurantListResult.Error -> prevState.copy(isError = true, isLoading = false, isIdle = false)
        }
    }

    private fun handleError(throwable: Throwable) {
        throwable.printStackTrace()
    }

    private fun loadRestaurantList(uiEvent: RestaurantListUiEvent) {
        when {
            uiEvent.isIdle -> return
            uiEvent.lat != null && uiEvent.lon != null && uiEvent.offset == 0 -> {
                actionSubject.onNext(RestaurantListAction.Load(uiEvent.lat, uiEvent.lon, uiEvent.offset, uiEvent.limit))
            }
            uiEvent.lat != null && uiEvent.lon != null && uiEvent.offset > 0 -> {
                actionSubject.onNext(RestaurantListAction.Paginate(uiEvent.lat, uiEvent.lon, uiEvent.offset, uiEvent.limit))
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

    @Suppress("UNCHECKED_CAST")
    class Factory: ViewModelProvider.NewInstanceFactory() {

        private val useCase = LoadRestaurantListUseCase(RestaurantListDataSource(), AppSchedulerProvider)

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return RestaurantListViewModel(useCase, AppSchedulerProvider) as T
        }

    }
}

