package dashlite.doordash.jho.com.dashlite.utils

import okhttp3.OkHttpClient
import retrofit2.CallAdapter
import retrofit2.Retrofit

/**
 * Created by JH on 1/12/19.
 */
fun httpClient(init: (OkHttpClient.Builder.() -> Unit)) : OkHttpClient =
        OkHttpClient.Builder().apply(init).build()

fun retrofitClient(init: (Retrofit.Builder.() -> Unit)) : Retrofit =
        Retrofit.Builder().apply(init).build()

fun Retrofit.Builder.callAdapter(adapter: CallAdapter.Factory): Retrofit.Builder =
    this.addCallAdapterFactory(adapter)

fun Retrofit.Builder.httpClient(client: OkHttpClient): Retrofit.Builder =
    this.client(client)

fun Retrofit.Builder.baseUrl(baseUrl: String): Retrofit.Builder =
    this.baseUrl(baseUrl)