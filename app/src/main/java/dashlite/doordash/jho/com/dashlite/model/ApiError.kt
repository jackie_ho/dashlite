package dashlite.doordash.jho.com.dashlite.model

/**
 * Created by JH on 1/13/19.
 */
data class ApiError(val throwable: Throwable)