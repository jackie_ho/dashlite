package dashlite.doordash.jho.com.dashlite.list

import dashlite.doordash.jho.com.dashlite.model.ApiError
import dashlite.doordash.jho.com.dashlite.model.Restaurant
import dashlite.doordash.jho.com.dashlite.utils.SchedulerProvider
import io.reactivex.Observable
import io.reactivex.ObservableTransformer

/**
 * Created by JH on 1/13/19.
 */
class LoadRestaurantListUseCase(val dataSource: RestaurantListRepository, val schedulerProvider: SchedulerProvider) {

    private val transformer = ObservableTransformer<List<Restaurant>, RestaurantListResult> { source ->
        source
            .map<RestaurantListResult> {
                RestaurantListResult.Success(it)
            }
            .onErrorReturn {
                RestaurantListResult.Error(ApiError(it))
            }
            .startWith(RestaurantListResult.Loading)
            .subscribeOn(schedulerProvider.io())
    }

    fun execute(actionObservable: Observable<RestaurantListAction>): Observable<RestaurantListResult> {
        return actionObservable
            .switchMap<RestaurantListResult> { action ->
                when (action) {
                    is RestaurantListAction.Load -> {
                        dataSource
                            .restaurantList(action.lat, action.lon, action.offset, action.limit)
                            .compose(transformer)
                    }
                    is RestaurantListAction.Paginate -> {
                        dataSource
                            .restaurantList(action.lat, action.lon, action.offset, action.limit)
                            .compose(transformer)
                    }
                }
            }
    }
}