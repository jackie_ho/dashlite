package dashlite.doordash.jho.com.dashlite.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import dashlite.doordash.jho.com.dashlite.R
import dashlite.doordash.jho.com.dashlite.model.Restaurant

/**
 * Created by JH on 1/13/19.
 */
class RestaurantListAdapter: ListAdapter<Restaurant, RestaurantViewHolder>(RestaurantDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_restaurant_list, parent, false)
        return RestaurantViewHolder(view)
    }

    override fun onBindViewHolder(holder: RestaurantViewHolder, position: Int) {
        holder.bindRestaurant(getItem(position))
    }
}


class RestaurantViewHolder(view: View): RecyclerView.ViewHolder(view) {

    private val iconImg = view.findViewById<ImageView>(R.id.restaurantCoverImage)
    private val nameText = view.findViewById<TextView>(R.id.restaurantNameText)
    private val descText = view.findViewById<TextView>(R.id.restaurantDescriptionText)
    private val statusText = view.findViewById<TextView>(R.id.restaurantStatusText)

    fun bindRestaurant(restaurant: Restaurant) {
        Picasso.get().load(restaurant.cover_img_url).into(iconImg)
        nameText.text = restaurant.name
        descText.text = restaurant.description
        statusText.text = restaurant.status
    }
}

class RestaurantDiffCallback : DiffUtil.ItemCallback<Restaurant>() {
    override fun areItemsTheSame(oldItem: Restaurant, newItem: Restaurant): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Restaurant, newItem: Restaurant): Boolean {
        return oldItem == newItem
    }
}