package dashlite.doordash.jho.com.dashlite

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.transaction
import dashlite.doordash.jho.com.dashlite.list.RestaurantListFragment

const val INIT_LAT = 37.422740
const val INIT_LON = -122.139956

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager.transaction {
                replace(R.id.container, RestaurantListFragment.newInstance(INIT_LAT, INIT_LON))
            }
        }
    }
}
