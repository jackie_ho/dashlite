package dashlite.doordash.jho.com.dashlite.list

import dashlite.doordash.jho.com.dashlite.model.ApiError
import dashlite.doordash.jho.com.dashlite.model.Restaurant

/**
 * Created by JH on 1/13/19.
 */

data class RestaurantListViewState(val isError: Boolean = false,
                                   val list: List<Restaurant> = emptyList(),
                                    val isLoading: Boolean = false,
                                   val isIdle: Boolean)

data class RestaurantListUiEvent(
    val lat: Double?,
    val lon: Double?,
    val offset: Int = 0,
    val limit: Int = 50,
    val isIdle: Boolean = false)

sealed class RestaurantListAction {
    class Load(val lat: Double, val lon: Double, val offset: Int, val limit: Int) : RestaurantListAction()
    class Paginate(val lat: Double, val lon: Double, val offset: Int, val limit: Int) : RestaurantListAction()

}

sealed class RestaurantListResult {
    object Loading : RestaurantListResult()
    data class Success(val restaurantList: List<Restaurant>) : RestaurantListResult()
    data class Error(val error: ApiError) : RestaurantListResult()
}

