package dashlite.doordash.jho.com.dashlite.list

import dashlite.doordash.jho.com.dashlite.Api
import dashlite.doordash.jho.com.dashlite.DashLiteApp
import dashlite.doordash.jho.com.dashlite.model.Restaurant
import io.reactivex.Observable

/**
 * Created by JH on 1/13/19.
 */

class RestaurantListDataSource : RestaurantListRepository {

    private val api: Api = DashLiteApp.retrofit.create(Api::class.java)

    override fun restaurantList(lat: Double, lon: Double, offset: Int, limit: Int): Observable<List<Restaurant>> =
        api.restaurantList(lat, lon, offset, limit)
}

interface RestaurantListRepository {
    fun restaurantList(lat: Double, lon: Double, offset: Int, limit : Int): Observable<List<Restaurant>>
}