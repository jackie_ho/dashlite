package dashlite.doordash.jho.com.dashlite.list


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dashlite.doordash.jho.com.dashlite.R
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_restaurant_list.*

private const val HOME_LAT = "lat"
private const val HOME_LON = "lon"

/**
 * A simple [Fragment] subclass.
 * Use the [RestaurantListFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class RestaurantListFragment : Fragment() {
    private var lat: Double? = null
    private var lon: Double? = null
    private lateinit var viewModel: RestaurantListViewModel

    private val uiSubject = PublishSubject.create<RestaurantListUiEvent>()
    private val disposables = CompositeDisposable()
    private val listAdapter = RestaurantListAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            lat = it.getDouble(HOME_LAT)
            lon = it.getDouble(HOME_LON)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        retainInstance = true
        return inflater.inflate(R.layout.fragment_restaurant_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        viewModel =
                ViewModelProviders.of(this, RestaurantListViewModel.Factory()).get(RestaurantListViewModel::class.java)
        viewModel.bindInput(uiSubject)
        disposables.add(viewModel.render()
            .subscribe { state ->
                progressBar.visibility = if (state.isLoading) View.VISIBLE else View.GONE

                if (state.isIdle) return@subscribe

                if (state.isError) {
                    Toast.makeText(context, "An error has occurred", Toast.LENGTH_SHORT).show()
                } else if (state.list.isNotEmpty()) {
                    listAdapter.submitList(state.list)
                }

            })
        if (savedInstanceState == null) uiSubject.onNext(RestaurantListUiEvent(lat, lon))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposables.clear()
    }

    private fun initViews() {
        with(restaurantListRecycler) {
            layoutManager = LinearLayoutManager(context)
            adapter = listAdapter
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        }
    }

    companion object {

        @JvmStatic
        fun newInstance(lat: Double, lon: Double) =
            RestaurantListFragment().apply {
                arguments = Bundle().apply {
                    putDouble(HOME_LAT, lat)
                    putDouble(HOME_LON, lon)
                }
            }
    }
}
