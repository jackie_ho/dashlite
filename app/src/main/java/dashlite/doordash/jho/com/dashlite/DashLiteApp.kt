package dashlite.doordash.jho.com.dashlite

import android.app.Application
import dashlite.doordash.jho.com.dashlite.utils.httpClient
import dashlite.doordash.jho.com.dashlite.utils.retrofitClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * Created by JH on 1/12/19.
 */

private const val BASE_URL = "https://api.doordash.com/"

class DashLiteApp : Application() {

    companion object {
        lateinit var retrofit: Retrofit
    }

    override fun onCreate() {
        super.onCreate()

        retrofit = retrofitClient {
            baseUrl(BASE_URL)
            addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            addConverterFactory(MoshiConverterFactory.create())
            client(httpClient {
                addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
            })
        }
    }



}